
### ALICE ADMIN TEMPLATE


**Alice**是谁？当然不是那个在童话里梦游仙境的小姑娘，更不是二次元中那个拥有超能力的少女。在这里，她是一个偶尔喜欢发脾气(BUG)的好伙伴。谦逊、优雅并且动作麻利，就这样陪伴于每个人(项目)的身边。

Who is Alice?Of course she is not the little girl in Wonderland.Neither is she the girl who has superpower in quadratic element.Here,she is the girl who occasionally has temper(BUG).She is humble,elegant,quick and neat.This is how she accompany everyone(PROGRAM).

她基于JAVA语言和成熟的开源项目，为中小型企业级软件和移动端提供安全性高、响应速度快并且配置简单易开发的基础后台管理系统。她不是一个产品，也不是平台更不是框架。准确的说只是一个开始，在任何平台只需简单的配置即可搭建起一个拥有基础功能的后台管理系统，再以插件或二次开发的形式进行业务功能扩展。

She is based on JAVA language and mature open source projects.It provides security,fast response,and easy to configure content management system for mall and medium sized enterprise business administration software.She is not a product,nor is it a platform.To be accurate,she is just a beginning.In any platform,in order to build a basic content management system,all you need is a simple configuration.Then to extend the functions just use plug-in or redevelop.


### 版本信息 / Versions

V0.1.3
增加手动锁屏功能；
修复了POM文件中缺少servlet-api和jsp-api的问题；
无用文件清理；
BUG修复；

V0.1.2
增加以RSA方式对密码进行加密传输；
修复了几个BUG；

V0.1.1
修复了一个导致页面显示错位的BUG；

V0.1.0
初始版本发布；


### DEMO

[传送门(测试用户：demo/demo)](http://alice.mayee.net)

[DEMO ONLINE(TEST USER:demo/demo)](http://alice.mayee.net)


更多信息请访问[MAYEE.NET](http://mayee.net/e)

FOR MORE INFORMATION,PLEASE VISIT[MAYEE.NET](http://mayee.net/e)



### leo-0.3.1.jar无法下载问题

已上传至OSC的MAVEN仓库，但依然无法下载请[猛击这里](http://mayee.net/b/bp/b35969036cf24c5b89a55abfab0d0ff1)


### 问题及建议

非常欢迎任何人提出建议或指出不足，可以发邮件到maye100@vip.163.com（无特殊情况都会当天回复）






