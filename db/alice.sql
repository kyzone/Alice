SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `alice` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `alice` ;

-- -----------------------------------------------------
-- Table `alice`.`tn_language`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `alice`.`tn_language` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `code` VARCHAR(5) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

INSERT INTO `alice`.`tn_language` VALUES(null,'zh_CN','简体中文');
INSERT INTO `alice`.`tn_language` VALUES(null,'zh_TW','繁體中文');
INSERT INTO `alice`.`tn_language` VALUES(null,'en_US','English');

-- -----------------------------------------------------
-- Table `alice`.`tn_user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `alice`.`tn_user` (
  `uuid` VARCHAR(40) NOT NULL ,
  `login_name` VARCHAR(20) NOT NULL UNIQUE,
  `login_password` VARCHAR(128) NOT NULL ,
  `name` VARCHAR(20) NOT NULL ,
  `phone` VARCHAR(20) NULL ,
  `email` VARCHAR(40) NOT NULL ,
  `photo_img` VARCHAR(255) NOT NULL ,
  `salt` VARCHAR(30) NOT NULL ,
  `status_code` INT NOT NULL ,
  `language_id` INT NOT NULL ,
  `register_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`) )
ENGINE = InnoDB;

-- replace(uuid(),'-','')
-- 账户alice/密码1111
INSERT INTO `alice`.`tn_user` VALUES(
'a91186be6d8311e5a3429d5aec8e066f','alice','b5a83d88ba883d9ae86773818f08bc50ffeceb85','Alice',
'13812345678','mayee@mayee.net','image/defPhoto200_200.jpg','353d8a8b0adc5493',1,1,null);

-- -----------------------------------------------------
-- Table `alice`.`tn_role`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `alice`.`tn_role` (
  `uuid` VARCHAR(40) NOT NULL ,
  `role_key` VARCHAR(20) NOT NULL UNIQUE ,
  `name` VARCHAR(20) NOT NULL ,
  `note` VARCHAR(100) NULL ,
  `permissions` VARCHAR(5000) NULL ,
  `create_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`) )
ENGINE = InnoDB;

-- -----------------------------------------------------
-- Table `alice`.`tr_user_role`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `alice`.`tr_user_role` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_uuid` VARCHAR(40) NOT NULL ,
  `role_uuid` VARCHAR(40) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;

-- INSERT INTO `alice`.`tr_user_role` VALUES(null,'a91186be6d8311e5a3429d5aec8e066f','0edb9c5fc8454923a427e4c64f35a5d3');



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
