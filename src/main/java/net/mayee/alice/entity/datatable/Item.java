package net.mayee.alice.entity.datatable;

import java.io.Serializable;

/**
 * Created by mayee on 15/11/19.
 */
public class Item implements Serializable {

    private String name;
    private String text;
    private String defText;

    public Item(String name, String text, String defText) {
        this.name = name;
        this.text = text;
        this.defText = defText;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDefText() {
        return defText;
    }

    public void setDefText(String defText) {
        this.defText = defText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Item item = (Item) o;

        return !(name != null ? !name.equals(item.name) : item.name != null);

    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
