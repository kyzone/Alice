package net.mayee.alice.entity.menu.nanbar;

import java.io.Serializable;

/**
 * Created by mayee on 15/9/20.
 */
public class Logo implements Serializable {

    private String href;
    private String imgSrc;
    private String alt;

    public Logo(String href, String imgSrc) {
        this.href = href;
        this.imgSrc = imgSrc;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Logo logo = (Logo) o;

        return href.equals(logo.href);

    }

    @Override
    public int hashCode() {
        return href.hashCode();
    }
}
