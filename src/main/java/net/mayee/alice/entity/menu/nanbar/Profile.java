package net.mayee.alice.entity.menu.nanbar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayee on 15/9/20.
 */
public class Profile {

    private String showPhoto;
    private String viewName;

    List<NavbarMenu> navbarMenuList = new ArrayList<NavbarMenu>();

    public Profile(String showPhoto, String viewName) {
        this.showPhoto = showPhoto;
        this.viewName = viewName;
    }

    public String getShowPhoto() {
        return showPhoto;
    }

    public void setShowPhoto(String showPhoto) {
        this.showPhoto = showPhoto;
    }

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    public List<NavbarMenu> getNavbarMenuList() {
        return navbarMenuList;
    }

    public void setNavbarMenuList(List<NavbarMenu> navbarMenuList) {
        this.navbarMenuList = navbarMenuList;
    }
}
