package net.mayee.alice.entity.permission;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mayee on 15/10/18.
 */
public class Permission {

    private String menuId;
    private String menuName;
    private List<Action> actionList = new ArrayList<Action>();

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public Permission(String menuId, String menuName) {
        this.menuId = menuId;
        this.menuName = menuName;
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public List<Action> getActionList() {
        return actionList;
    }

    public void setActionList(List<Action> actionList) {
        this.actionList = actionList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Permission that = (Permission) o;

        return !(menuId != null ? !menuId.equals(that.menuId) : that.menuId != null);

    }

    @Override
    public int hashCode() {
        return menuId != null ? menuId.hashCode() : 0;
    }
}
