package net.mayee.alice.filter.web;

import java.io.UnsupportedEncodingException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.authz.AuthorizationFilter;

public class InputStreamWebFilter extends AuthorizationFilter {

    @Override
    protected boolean isAccessAllowed(ServletRequest request,
            ServletResponse response, Object obj)
            throws UnsupportedEncodingException {

        return true;
    }
}
