package net.mayee.alice.functions.ui;

import jetbrick.template.JetAnnotations;
import net.mayee.alice.common.Definiens;
import net.mayee.alice.entity.coder.UserStatusItem;
import net.mayee.alice.entity.menu.sidebar.SidebarMenu;
import net.mayee.alice.filter.ShiroAdminDbRealm.ShiroUser;
import net.mayee.alice.functions.base.BaseFunctions;
import net.mayee.common.AliceHelper;
import net.mayee.common.CoderHelper;
import net.mayee.common.MenuHelper;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.support.RequestContext;

import javax.servlet.http.HttpServletRequest;

@JetAnnotations.Functions
public class UIFunction {

    public static String getUserStatusText(int code) {
        UserStatusItem usi = CoderHelper.getInstance().getObjectByUserStatusCode("" + code);
        return BaseFunctions.getMessage(usi.getText(), usi.getDefText());
    }

    public static String printUserStatusTextLabel(HttpServletRequest request, int code) {
        UserStatusItem usi = CoderHelper.getInstance().getObjectByUserStatusCode("" + code);
        String text;
        if (request == null) {
            text = BaseFunctions.getMessage(usi.getText(), usi.getDefText());
        } else {
            text = new RequestContext(request).getMessage(usi.getText(), usi.getDefText());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("<span class=\"label ")
                .append(usi.getLabelClass())
                .append("\">")
                .append(text)
                .append("</span>");
        return sb.toString();
    }

    public static String printUserStatusTextLabel(int code) {
        return printUserStatusTextLabel(null, code);
    }

    public static ShiroUser getShrioUser() {
        Subject currentUser = SecurityUtils.getSubject();
        return (ShiroUser) currentUser.getPrincipal();
    }

    public static boolean isAdmin() {
        Subject currentUser = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
        return shiroUser.getStatusCode() == Definiens.STATUS_CODE_USER_SUPER_ADMIN
                || shiroUser.getStatusCode() == Definiens.STATUS_CODE_USER_ADMIN;
    }

    public static boolean isNotAdmin() {
        Subject currentUser = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
        return shiroUser.getStatusCode() != Definiens.STATUS_CODE_USER_SUPER_ADMIN
                && shiroUser.getStatusCode() != Definiens.STATUS_CODE_USER_ADMIN;
    }

    public static boolean isNormal() {
        Subject currentUser = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
        return shiroUser.getStatusCode() == Definiens.STATUS_CODE_USER_NORMAL;
    }

    public static boolean isSuperAdmin() {
        Subject currentUser = SecurityUtils.getSubject();
        ShiroUser shiroUser = (ShiroUser) currentUser.getPrincipal();
        return shiroUser.getStatusCode() == Definiens.STATUS_CODE_USER_SUPER_ADMIN;
    }

    public static boolean isPermission(String str) {
        if (isAdmin()) {
            return true;
        } else {
            return SecurityUtils.getSubject().isPermitted(str);
        }
    }

    public static String props(String key) {
        return AliceHelper.getInstance().getJoddProps().getValue(key);
    }

    public static String printTitle() {
        StringBuilder sb = new StringBuilder();
        sb.append(props("system.product.name")).append(" | ");

        SidebarMenu sbm = MenuHelper.getInstance().getActiveSidebarMenu();
        if (sbm != null) {
            sb.append(sbm.getName());
        }
        return sb.toString();
    }

    public static String printUserEditActions(String uid, int statusCode) {
        StringBuilder sb = new StringBuilder();
        if(Definiens.STATUS_CODE_USER_SUPER_ADMIN == statusCode){
            return "";
        }
        if (isAdmin()) {
            sb.append("<div class=\"actions\">")
                    .append("<div class=\"btn-group\">")
                    .append("<a class=\"btn btn-sm blue dropdown-toggle\" href=\"javascript:;\" data-toggle=\"dropdown\">")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.actions"))
                    .append(" <i class=\"fa fa-angle-down\"></i>")
                    .append("</a>")
                    .append("<ul class=\"dropdown-menu pull-right\">")
                    .append("<li><a id=\"delBtn\">")
                    .append("<i class=\"fa fa-trash-o\"></i> ")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.delete"))
                    .append(" </a></li>");
        } else {
            if (isPermission("user:D")) {
                sb.append("<div class=\"actions\">")
                        .append("<div class=\"btn-group\">")
                        .append("<a class=\"btn btn-sm blue dropdown-toggle\" href=\"javascript:;\" data-toggle=\"dropdown\">")
                        .append(BaseFunctions.getMessage("net.mayee.alice.btn.actions"))
                        .append(" <i class=\"fa fa-angle-down\"></i>")
                        .append("</a>")
                        .append("<ul class=\"dropdown-menu pull-right\">")
                        .append("<li><a id=\"delBtn\">")
                        .append("<i class=\"fa fa-trash-o\"></i> ")
                        .append(BaseFunctions.getMessage("net.mayee.alice.btn.delete"))
                        .append(" </a></li>");
            }else{
                return "";
            }
        }
        if (isSuperAdmin()) {
            sb.append("<li class=\"divider\"></li>");
            if (statusCode == Definiens.STATUS_CODE_USER_NORMAL
                    || statusCode == Definiens.STATUS_CODE_USER_LOCKED) {
                sb.append("<li><a href=\"javascript:;\" id=\"onABtn\">")
                        .append("<i class=\"fa fa-paw\"></i> ")
                        .append(BaseFunctions.getMessage("net.mayee.alice.btn.on_admin"))
                        .append(" </a></li>");
            } else if (statusCode == Definiens.STATUS_CODE_USER_ADMIN
                    || statusCode == Definiens.STATUS_CODE_USER_LOCKED) {
                sb.append("<li><a href=\"javascript:;\" id=\"offABtn\">")
                        .append("<i class=\"fa fa-minus\"></i> ")
                        .append(BaseFunctions.getMessage("net.mayee.alice.btn.off_admin"))
                        .append(" </a></li>");
            }
        }
        sb.append("</ul>").append("</div></div>");
        return sb.toString();
    }

    public static String printRoleEditActions(String uid) {
        StringBuilder sb = new StringBuilder();
        if (isAdmin() || isSuperAdmin()) {
            sb.append("<div class=\"actions\">")
                    .append("<div class=\"btn-group\">")
                    .append("<a class=\"btn btn-sm blue dropdown-toggle\" href=\"javascript:;\" data-toggle=\"dropdown\">")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.actions"))
                    .append(" <i class=\"fa fa-angle-down\"></i>")
                    .append("</a>")
                    .append("<ul class=\"dropdown-menu pull-right\">")
                    .append("<li><a href=\"").append(BaseFunctions.getPath()).append("/sys/role/UR/").append(uid)
                    .append("\"><i class=\"fa fa-users\"></i> ")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.edit_user"))
                    .append(" </a></li>")
                    .append("<li><a href=\"").append(BaseFunctions.getPath()).append("/sys/role/RP/").append(uid)
                    .append("\"><i class=\"fa fa-key\"></i> ")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.edit_permission"))
                    .append(" </a></li>")
                    .append("<li class=\"divider\"></li>")
                    .append("<li><a id=\"delBtn\">")
                    .append("<i class=\"fa fa-trash-o\"></i> ")
                    .append(BaseFunctions.getMessage("net.mayee.alice.btn.delete"))
                    .append(" </a></li>")
                    .append("</ul>")
                    .append("</div></div>");
        } else {
            Subject subject = SecurityUtils.getSubject();
            boolean EDIT_USERS = subject.isPermitted("role:ROLE_PERMISSION_U");
            boolean EDIT_PERMISSION = subject.isPermitted("role:ROLE_PERMISSION_U");
            boolean D = subject.isPermitted("role:D");
            if (EDIT_USERS == true || EDIT_PERMISSION == true || D == true) {
                sb.append("<div class=\"actions\">")
                        .append("<div class=\"btn-group\">")
                        .append("<a class=\"btn btn-sm blue dropdown-toggle\" href=\"javascript:;\" data-toggle=\"dropdown\">")
                        .append(BaseFunctions.getMessage("net.mayee.alice.btn.actions"))
                        .append(" <i class=\"fa fa-angle-down\"></i>")
                        .append("</a>")
                        .append("<ul class=\"dropdown-menu pull-right\">");
                if (EDIT_USERS) {
                    sb.append("<li><a href=\"").append(BaseFunctions.getPath()).append("/sys/role/UR/").append(uid)
                            .append("\"><i class=\"fa fa-users\"></i> ")
                            .append(BaseFunctions.getMessage("net.mayee.alice.btn.edit_user"))
                            .append(" </a></li>");
                }
                if (EDIT_PERMISSION) {
                    sb.append("<li><a href=\"").append(BaseFunctions.getPath()).append("/sys/role/RP/").append(uid)
                            .append("\"><i class=\"fa fa-key\"></i> ")
                            .append(BaseFunctions.getMessage("net.mayee.alice.btn.edit_permission"))
                            .append(" </a></li>");
                }
                if ((EDIT_USERS == true || EDIT_PERMISSION == true) && D == true) {
                    sb.append("<li class=\"divider\"></li>");
                }
                if (D == true) {
                    sb.append("<li><a id=\"delBtn\">")
                            .append("<i class=\"fa fa-trash-o\"></i> ")
                            .append(BaseFunctions.getMessage("net.mayee.alice.btn.delete"))
                            .append(" </a></li>");
                }
                sb.append("</ul>").append("</div></div>");
            }
        }
        return sb.toString();
    }

}

