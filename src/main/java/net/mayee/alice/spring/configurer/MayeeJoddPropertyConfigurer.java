package net.mayee.alice.spring.configurer;

import jodd.props.Props;
import net.mayee.common.AliceHelper;
import net.mayee.leo.LeoEncrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.IOException;
import java.util.Properties;



public class MayeeJoddPropertyConfigurer extends PropertyPlaceholderConfigurer {

    private final Logger LOGGER = LoggerFactory.getLogger(MayeeJoddPropertyConfigurer.class);
    private static final String JDBC_USERNAME = "jdbc.username";
    private static final String JDBC_PASSWORD = "jdbc.password";
    private static final String JDBC_URL = "jdbc.url";

    private static final String JDBC_DRIVER = "jdbc.driver";
    private static final String JDBC_INITIAL_SIZE = "jdbc.initial.size";
    private static final String JDBC_MAX_ACTIVE = "jdbc.max.active";

    private static final String APPLICATION_PROPS_FILE = "classpath:config/application.properties";

    protected void processProperties(
            ConfigurableListableBeanFactory beanFactory, Properties props)
            throws BeansException {

        Props p = new Props();
        ResourceLoader loader = new DefaultResourceLoader();
        Resource resource_app = loader.getResource(APPLICATION_PROPS_FILE);
        if (resource_app.exists()) {
            try {
                p.load(resource_app.getInputStream(), "UTF-8");
            } catch (IOException e) {
                LOGGER.error("**** MayeeJoddPropertyConfigurer -> processProperties fail! ****", e);
            }
        }

        AliceHelper.getInstance().setJoddProps(p);
        AliceHelper.getInstance().setApplicationConfigFileClassPath(APPLICATION_PROPS_FILE);

        Properties props_new = new Properties();
        props_new.setProperty(JDBC_INITIAL_SIZE, p.getValue(JDBC_INITIAL_SIZE));
        props_new.setProperty(JDBC_MAX_ACTIVE, p.getValue(JDBC_MAX_ACTIVE));

        String jdbc_url = p.getValue(JDBC_URL);
        String jdbc_username = p.getValue(JDBC_USERNAME);
        String jdbc_password = p.getValue(JDBC_PASSWORD);

        String key = p.getValue("encrypt.aes.key");
        if("true".equals(p.getValue("jdbc.encrypt"))){
            jdbc_url = LeoEncrypt.aesDecrypt(jdbc_url, key);
            jdbc_username = LeoEncrypt.aesDecrypt(jdbc_username, key);
            jdbc_password = LeoEncrypt.aesDecrypt(jdbc_password, key);
        }
        props_new.setProperty(JDBC_URL, jdbc_url);
        props_new.setProperty(JDBC_USERNAME, jdbc_username);
        props_new.setProperty(JDBC_PASSWORD, jdbc_password);

        super.processProperties(beanFactory, props_new);

    }
}
