package net.mayee.alice.spring.interceptor;

import net.mayee.alice.common.Definiens;
import net.mayee.alice.filter.ShiroAdminDbRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LockScreenInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        Subject currentUser = SecurityUtils.getSubject();
        ShiroAdminDbRealm.ShiroUser shiroUser = (ShiroAdminDbRealm.ShiroUser) currentUser.getPrincipal();
        if(shiroUser != null){
            System.out.println("getRequestURL:"+request.getRequestURL());
            if(shiroUser.getIsLockScreen() == Definiens.STATUS_CODE_LOCKSCREEN){
                System.out.println("Result:no");
                return false;
            }
            System.out.println("Result:yes");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}
