package net.mayee.alice.web.controller.datatable;

import net.mayee.alice.entity.datatable.Item;
import net.mayee.alice.web.controller.base.BaseController;
import net.mayee.common.DatatablesHelper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Controller
public class DatatableController extends BaseController {

    @RequestMapping(value = "/dt/ajax/lag")
    public void dtLag(HttpServletResponse response, HttpServletRequest request) throws IOException, JSONException {
        JSONObject result = new JSONObject();

        Map<String, Object> languageMap = DatatablesHelper.getInstance().getLanguageMap();
        Set<String> keySet = languageMap.keySet();
        for (String name : keySet) {
            Object obj = languageMap.get(name);
            if (obj instanceof Item) {
                Item item = (Item)obj;
                result.put(name, this.getMessage(item.getText(), item.getDefText()));
            }else if(obj instanceof HashMap){
                JSONObject paginate = new JSONObject();
                Map<String, Item> paginateMap = (HashMap)obj;
                Set<String> keySet2 = paginateMap.keySet();
                for (String name2 : keySet2) {
                    Item item2 = paginateMap.get(name2);
                    paginate.put(name2, this.getMessage(item2.getText(), item2.getDefText()));
                }
                result.put("oPaginate", paginate);
            }
        }
        request.setAttribute("reJSON", result.toString());
    }

}
