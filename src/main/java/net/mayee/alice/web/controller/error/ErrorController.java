package net.mayee.alice.web.controller.error;

import jodd.log.Logger;
import jodd.log.LoggerFactory;
import net.mayee.alice.web.controller.base.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ErrorController extends BaseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ErrorController.class);

    @RequestMapping(value = "/404")
    public String error404(Model model) {
        return "error/404";
    }

    @RequestMapping(value = "/500")
    public String error500(Model model) {
        return "error/500";
    }

}
