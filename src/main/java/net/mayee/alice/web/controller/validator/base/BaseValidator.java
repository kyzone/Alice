package net.mayee.alice.web.controller.validator.base;

import java.util.regex.Pattern;

public class BaseValidator {

	protected static final Pattern EMAIL_PATTERN = Pattern
			.compile("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");

}
