var LockScreenLogin = function() {

    var lockScreenLogin = function() {

        var rsa_m = $("#rsa_m");
        var rsa_e = $("#rsa_e");
        var pwObj = $("#password");

        $('.lock-form').validate({
            errorElement: 'span',
            errorClass: 'help-block',
            focusInvalid: false,
            rules: {
                password: {
                    required: true
                }
            },

            messages: {
                password: {
                    required: "请输入密码。"
                }
            },

            invalidHandler: function(event, validator) {
                $('.alert-danger', $('.lock-form')).show();
            },

            highlight: function(element) {
                $(element)
                    .closest('.form-group').addClass('has-error');
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            errorPlacement: function(error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },

            submitHandler: function(form) {
                setMaxDigits(130);
                var key = new RSAKeyPair(rsa_e.val(), "", rsa_m.val());
                pwObj.val(encryptedString(key, pwObj.val()));
                form.submit();
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });

    }





    return {
        init: function() {

            lockScreenLogin();

        }

    };

}();